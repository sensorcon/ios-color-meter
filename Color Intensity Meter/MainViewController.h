//
//  MainViewController.h
//  Color Intensity Meter
//
//  Created by Mark Rudolph on 12/16/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MainViewController : BaseViewController <AVAudioPlayerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *irisImage;
@property (weak, nonatomic) IBOutlet UIImageView *streamIndicator;
@property BOOL isStreaming;

@property (weak, nonatomic) IBOutlet UILabel *luxValue;
@property (weak, nonatomic) IBOutlet UILabel *redValue;
@property (weak, nonatomic) IBOutlet UILabel *greenValue;
@property (weak, nonatomic) IBOutlet UILabel *blueValue;

@property NSTimer *streamer;

// Audio
@property (nonatomic, retain) AVAudioPlayer *shutter;

- (IBAction)measureButtonDown:(id)sender;

- (IBAction)measureButtonPressed:(id)sender;
- (IBAction)streamButtonPressed:(id)sender;


-(void)setColorValues:(float)lux :(float)red :(float)green :(float)blue;
-(void)setStreamImages:(BOOL)onOff;
-(void)blinkIris;

@end
