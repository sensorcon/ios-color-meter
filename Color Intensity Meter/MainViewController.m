//
//  MainViewController.m
//  Color Intensity Meter
//
//  Created by Mark Rudolph on 12/16/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize luxValue;
@synthesize redValue;
@synthesize greenValue;
@synthesize blueValue;

@synthesize irisImage;
@synthesize streamIndicator;

@synthesize isStreaming;
@synthesize streamer;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Off by default
    [self setColorValues:0 :0 :0 :0];
    [self setIsStreaming:NO];
    [self setStreamImages:isStreaming];
    
    // Get our audio ready
    NSString *path = [[NSBundle mainBundle] pathForResource:@"shutter" ofType:@"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
    
    self.shutter = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:NULL];
    [self.shutter setVolume:1];
    [self.shutter setDelegate:self];
    [self.shutter prepareToPlay];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // If not (or no longer) connected, reset display
    if ([self.delegate.myDrone.dronePeripheral state] != CBPeripheralStateConnected) {
        [self setColorValues:0 :0 :0 :0];
        [self setIsStreaming:NO];
        [self setStreamImages:isStreaming];
    }
    
    [self.delegate blinkLEDs];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)blinkIris {
    
}

-(void)setColorValues:(float)lux :(float)red :(float)green :(float)blue {
    [luxValue setText:[NSString stringWithFormat:@"LUX: %.0f", lux]];
    [redValue setText:[NSString stringWithFormat:@"R: %.0f", red]];
    [greenValue setText:[NSString stringWithFormat:@"G: %.0f", green]];
    [blueValue setText:[NSString stringWithFormat:@"B: %.0f", blue]];
}

-(void)setStreamImages:(BOOL)onOff {
    if (onOff) {
        [irisImage setImage:[UIImage imageNamed:@"lense_open.png"]];
        [streamIndicator setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [irisImage setImage:[UIImage imageNamed:@"lense_closed.png"]];
        [streamIndicator setImage:[UIImage imageNamed:@"led_off.png"]];
    }
}

- (IBAction)measureButtonDown:(id)sender {
    // Iris image is open while button is down
    [self setStreamImages:YES];
    
    // Measuring will disable any streaming
    if (streamer !=nil) {
        [streamer invalidate];
        streamer = nil;
    }
    
    // Trigger the measurement
    // (or enable if not enabled, which will trigger the first measurement
    if ([self.delegate.myDrone rgbcStatus]) {
        [self.delegate.myDrone measureRGBC];
    }
    else {
        [self.delegate.myDrone enableRGBC];
    }
    
    // Play the sound
    [self.shutter play];

}

- (IBAction)measureButtonPressed:(id)sender {
    // Iris image is closed when button is up
    [self setStreamImages:NO];
    
}

- (IBAction)streamButtonPressed:(id)sender {
    if (isStreaming) {
        // Shut it down
        if (streamer !=nil) {
            [streamer invalidate];
            streamer = nil;
        }
        [self setStreamImages:NO];
    }
    else {
        // Turn it on
        [self setStreamImages:YES];
        if (![self.delegate.myDrone rgbcStatus]) {
            [self.delegate.myDrone enableRGBC];
        }
        streamer = [NSTimer scheduledTimerWithTimeInterval:0.75 target:self.delegate.myDrone selector:@selector(measureRGBC) userInfo:nil repeats:YES];
    }
    
    // Toggle isStreaming
    [self setIsStreaming:!isStreaming];
}

-(void)doOnRGBCEnabled {
    [self.delegate.myDrone measureRGBC];
}

-(void)doOnRGBCMeasured {
    [self setColorValues:self.delegate.myDrone.measuredRGBCIlluminanceInLux
                        :self.delegate.myDrone.measuredRGBCRedChannel
                        :self.delegate.myDrone.measuredRGBCGreenChannel
                        :self.delegate.myDrone.measuredRGBCBlueChannel];
}
@end
